instances:
  ERGOM:
    model: iow/ergom
    parameters: 
      critical_stress : 0.016     ! critical shear stress for sediment erosion  (N/m2 )
      cya0            : 9.0E-8    ! seed concentration for diazotroph cyanobacteria  (mol/kg )
      din_min_lpp     : 1.0E-6    ! DIN half saturation constant for large-cell phytoplankton growth  (mol/kg )
      din_min_spp     : 1.6E-7    ! DIN half saturation constant for small-cell phytoplankton growth  (mol/kg )
      dip_min_cya     : 1.0E-8    ! DIP half saturation constant for diazotroph cyanobacteria growth  (mol/kg )
      epsilon         : 4.5E-17   ! no division by 0
      food_min_zoo    : 4.108E-6  ! Ivlev phytoplankton concentration for zooplankton grazing  (mol/kg )
      gamma0          : 0.0521     ! light attenuation parameter (opacity of clear water)  (1/m ), 
      gamma1          : 58.0      ! light attenuation parameter (opacity of POM containing chlorophyll)  (m**2/mol )
      gamma2          : 53.2      ! light attenuation parameter (opacity of POM detritus)  (m**2/mol )
      gamma3          : 12.6      ! light attenuation parameter (opacity of DON)  (m**2/mol )
      gamma4          : 94.8      ! light attenuation parameter (opacity of CDOM)  (m**2/mol )
      h2s_min_po4_liber : 1.0E-6  ! minimum h2s concentration for liberation of iron phosphate from the sediment  (mol/kg )
      ips_threshold   : 0.1       ! threshold for increased PO4 burial  (mol/m**2 )
      ips_cl          : 0.02025   ! iron phosphate in sediment closure parameter  (mol/m2 )
      k_h2s_no3       : 800000.0  ! reaction constant h2s oxidation with no3  (kg/mol/day )
      k_h2s_o2        : 800000.0  ! reaction constant h2s oxidation with o2  (kg/mol/day )
      k_sul_no3       : 20000.0   ! reaction constant sul oxidation with no3  (kg/mol/day )
      k_sul_o2        : 20000.0   ! reaction constant sul oxidation with o2  (kg/mol/day )
      light_opt_cya   : 50.0      ! optimal light for diazotroph cyanobacteria growth  (W/m**2 )
      light_opt_lpp   : 35.0      ! optimal light for large-cell phytoplankton growth  (W/m**2 )
      light_opt_spp   : 50.0      ! optimal light for small-cell phytoplankton growth  (W/m**2 )
      lpp0            : 4.5E-9    ! seed concentration for large-cell phytoplankton  (mol/kg )
      no3_min_sed_denit : 1.423E-7  ! nitrate half-saturation concentration for denitrification in the water column  (mol/kg )
      no3_min_det_denit : 1.0E-9    ! minimum no3 concentration for recycling of detritus using nitrate (denitrification)
      o2_min_det_resp : 1.0E-6    ! oxygen half-saturation constant for detritus recycling  (mol/kg )
      o2_min_nit      : 3.75E-6   ! oxygen half-saturation constant for nitrification  (mol/kg )
      o2_min_po4_retent : 0.0000375  ! oxygen half-saturation concentration for retension of phosphate during sediment denitrification  (mol/kg )
      o2_min_sed_resp : 0.000064952  ! oxygen half-saturation constant for recycling of sediment detritus using oxygen  (mol/kg )
      patm_co2        : 38.0      ! atmospheric partial pressure of CO2  (Pa )
      q10_det_rec     : 0.15      ! q10 rule factor for recycling  (1/K )
      q10_doc_rec     : 0.069     ! q10 rule factor for DOC recycling  (1/K )
      q10_h2s         : 0.0693    ! q10 rule factor for oxidation of h2s and sul  (1/K )
      q10_nit         : 0.11      ! q10 rule factor for nitrification  (1/K )
      q10_sed_rec     : 0.175     ! q10 rule factor for detritus recycling in the sediment  (1/K )
      r_biores        : 0.015     ! bio-resuspension rate  (1/day )
      r_cya_assim     : 0.75      ! maximum rate for nutrient uptake of diazotroph cyanobacteria  (1/day )
      r_cya_resp      : 0.01      ! respiration rate of cyanobacteria to ammonium  (1/day )
      r_det_rec       : 0.003     ! recycling rate (detritus to ammonium) at 0°C  (1/day )
      r_ips_burial    : 0.0018    ! final burial rate for PO4  (1/day )
      r_ips_ero       : 6.0       ! erosion rate for iron PO4  (1/day )
      r_ips_liber     : 0.1       ! PO4 liberation rate under anoxic conditions  (1/day )
      r_lpp_assim     : 1.38      ! maximum rate for nutrient uptake of large-cell phytoplankton  (1/day )
      r_lpp_resp      : 0.075     ! respiration rate of large phytoplankton to ammonium  (1/day )
      r_nh4_nitrif    : 0.05      ! nitrification rate at 0°C  (1/day )
      r_pp_mort       : 0.03      ! mortality rate of phytoplankton  (1/day )
      r_cya_mort_diff : 40.0      ! enhanced cya mortality due to strong turbulence
      r_cya_mort_thresh : 0.02      ! diffusivity threshold for enhanced cyano mortality
      r_sed_ero       : 6.0       ! maximum sediment detritus erosion rate  (1/day )
      r_sed_rec       : 0.003     ! maximum recycling rate for sedimentary detritus  (1/d )
      r_sed_poc_rec   : 0.0005    ! maximum recycling rate for sedimentary POC  (1/d )
      r_spp_assim     : 0.4       ! maximum rate for nutrient uptake of small-cell phytoplankton  (1/day )
      r_spp_resp      : 0.0175    ! respiration rate of small phytoplankton to ammonium  (1/day )
      r_zoo_graz      : 0.5       ! maximum zooplankton grazing rate  (1/day )
      r_zoo_mort      : 0.03      ! mortality rate of zooplankton  (1/day )
      r_zoo_resp      : 0.01      ! respiration rate of zooplankton  (1/day )
      rfr_c           : 6.625     ! redfield ratio C/N
      rfr_h           : 16.4375   ! redfield ratio H/N
      rfr_o           : 6.875     ! redfield ratio O/N
      rfr_p           : 0.0625    ! redfield ratio P/N
      rfr_cp          : 106.0     ! redfield ratio C/P
      sali_max_cya    : 8.0       ! upper salinity limit - diazotroph cyanobacteria  (psu )
      sali_min_cya    : 4.0       ! lower salinity limit - diazotroph cyanobacteria  (psu )
      nit_max_cya     : 5.0E-7    ! limits cyano growth in DIN reach environment
      nit_switch_cya  : 8.0       ! strengs of DIN control for cyano growth
      sed_max         : 1.0       ! maximum sediment detritus concentration that feels erosion  (mol/m**2 )
      sed_burial      : 1.0       ! maximum sediment load before burial
      spp0            : 4.5E-9    ! seed concentration for small-cell phytoplankton  (mol/kg )
      temp_min_cya    : 13.5      ! lower temperature limit - diazotroph cyanobacteria  (°C )
      temp_switch_cya : 4.0       ! strengs of temperature control for cyano growth
      temp_min_spp    : 10.0      ! lower temperature limit - small-cell phytoplankton  (°C )
      temp_opt_zoo    : 20.0      ! optimal temperature for zooplankton grazing  (°C )
      w_cya           : 1.0       ! vertical speed of diazotroph cyanobacteria  (m/day )
      w_det           : -4.5      ! vertical speed of detritus  (m/day )
      w_ipw           : -1.0      ! vertical speed of suspended iron PO4  (m/day )
      w_det_sedi      : -2.25     ! sedimentation velocity (negative for downward)  (m/day )
      w_ipw_sedi      : -0.5      ! sedimentation velocity for iron PO4  (m/day )
      w_lpp           : -0.5      ! vertical speed of large-cell phytoplankton  (m/day )
      w_n2_stf        : 5.0       ! piston velocity for n2 surface flux  (m/d )
      w_o2_stf        : 5.0       ! piston velocity for oxygen surface flux  (m/d )
      zoo0            : 4.5E-9    ! seed concentration for zooplankton  (mol/kg )
      zoo_cl          : 9.0E-8    ! zooplankton closure parameter  (mol/kg )
      don_fraction    : 0.0       ! fraction of DON in respiration products
      r_poc_rec       : 0.003     ! recycling rate (poc to dic) at 0°C  (1/day )
      r_pocp_rec      : 0.002     ! recycling rate (pocp to dic and po4) at 0°C  (1/day )
      r_pocn_rec      : 0.002     ! recycling rate (pocn to dic and nh4) at 0°C  (1/day )
      w_poc           : -0.2      ! vertical speed of poc  (m/day )
      w_poc_sedi      : -0.1      ! sedimentation velocity (negative for downward)  (m/day )
      w_pocp          : -0.1      ! vertical speed of pocp  (m/day )
      w_pocp_sedi     : -0.05     ! sedimentation velocity (negative for downward)  (m/day )
      w_pocn          : -0.1      ! vertical speed of pocn  (m/day )
      w_pocn_sedi     : -0.05     ! sedimentation velocity (negative for downward)  (m/day )
      fac_doc_assim_lpp : 1.0       ! factor modifying DOC assimilation rate of large phytoplankton LPP
      fac_doc_assim_cya : 1.0       ! factor modifying DOC assimilation rate of cyanobacteria
      fac_doc_assim_spp : 1.0       ! factor modifying DOC assimilation rate of small phytoplankton SPP
      fac_dop_assim   : 0.5       ! factor modifying assimilation rate for POCP production
      fac_don_assim   : 1.0       ! factor modifying assimilation rate for POCN production
      fac_enh_rec     : 10.0      ! enhance recyclig of DON,POCN/DOP,POCP in case of limiting DIN/DIP
      ret_po4_1       : 0.1       ! PO4 retension in oxic sediments
      ret_po4_2       : 0.5       ! additional PO4 retension in oxic sediments of the Bothnian Sea
      ret_po4_3       : 0.13      ! additional PO4 retension in oxic sediments of the Bothnian Sea
      frac_denit_scal : 1.0       ! scaling frac_denit_sed
      reduced_rec     : 0.8       ! decrease recycling in sed under anoxia by reduce_rec
      martin_fac_poc  : 0.01      !  (1/d ), depth dependence of POC sinking speed
      r_doc2poc       : 0.01      ! POC formation rate
      r_don2pocn      : 0.01      ! POCN formation rate
      r_dop2pocp      : 0.01      ! POCP formation rate
      r_doc_rec       : 0.001     ! recycling rate (doc to dic) at 0°C  (1/day )
      r_don_rec       : 0.001     ! recycling rate (don to dic and NH4) at 0°C  (1/day )
      r_dop_rec       : 0.001     ! recycling rate (dop to dic and PO4) at 0°C  (1/day )
      fac_ips_burial  : 0.5       ! reduced burial of t_ips, mimicing resolving iron-P complexes in deeper sediment and subsequent upward PO4 flux
      r_cdom_decay    : 0.0035    ! decay rate of cdom
      r_cdom_light    : 40.0      ! PAR intensity controling CDOM decay
      t_n2_initial    : 1.0e-5  ! dissolved molecular nitrogen
      t_o2_initial    : 4.5e-4  ! dissolved oxygen
      t_dic_initial   : 1.6e-3  ! dissolved inorganic carbon, treated as carbon dioxide
      t_nh4_initial   : 1.0e-5  ! ammonium
      t_no3_initial   : 1.0e-5  ! nitrate
      t_po4_initial   : 1.0e-5  ! phosphate
      t_spp_initial   : 1.0e-5  ! small-cell phytoplankton
      t_zoo_initial   : 1.0e-5  ! zooplankton
      t_h2s_initial   : 1.0e-5  ! hydrogen sulfide
      t_sul_initial   : 1.0e-5  ! sulfur
      t_alk_initial   : 2.0e-3  ! total alkalinity
      t_ipw_initial   : 1.0e-5  ! suspended iron phosphate
      t_lpp_initial   : 1.0e-5  ! large-cell phytoplankton
      t_cya_initial   : 1.0e-5  ! diazotroph cyanobacteria
      t_det_initial   : 1.0e-5  ! detritus
      t_poc_initial   : 1.0e-5  ! particulate organic carbon
      t_pocp_initial  : 1.0e-5  ! phosphorus in particulate organic carbon in Redfield ratio
      t_pocn_initial  : 1.0e-5  ! nitrogen in particulate organic carbon in Redfield ratio
      t_doc_initial   : 1.0e-5  ! dissolved organic carbon
      t_dop_initial   : 1.0e-5  ! phosphorus in dissolved organic carbon in Redfield ratio
      t_don_initial   : 1.0e-5  ! nitrogen in dissolved organic carbon in Redfield ratio
      t_cdom_initial  : 0.1e-5  ! colored dissolved organic carbon
      t_sed_initial   : 1.0e-5  ! sediment detritus
      t_ips_initial   : 1.0e-5  ! iron phosphate in sediment
      t_sed_poc_initial : 1.0e-5  ! sediment particular carbon
      t_sed_pocn_initial : 1.0e-5  ! sediment particular organic N+C
      t_sed_pocp_initial : 1.0e-5  ! sediment particular organic P+C

